﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Tilemap))]
public class CollisionMap : MonoBehaviour
{

    public int MapWidth = 20;
    public int MapHeight = 20;

    [HideInInspector] public bool[,] CollisionTable;
    [HideInInspector] public Vector3Int tileMapLocation;
    [HideInInspector] public BoundsInt tileMapCellBounds;

    void Start()
    {
        Tilemap tileMap = GetComponent<Tilemap>();

        tileMapCellBounds = tileMap.cellBounds;

        tileMapLocation = new Vector3Int((int)tileMap.cellBounds.position.x, (int)tileMap.cellBounds.position.y, 0);
        Vector3Int tileLocation = new Vector3Int((int)tileMap.cellBounds.position.x, (int)tileMap.cellBounds.position.y, 0);

        CollisionTable = new bool[tileMap.size.y, tileMap.size.x];
        for (int y = 0; y < tileMap.size.y; y++)
        {
            tileLocation.x = (int)tileMap.cellBounds.position.x;
            for (int x = 0; x < tileMap.size.x; x++)
            {
                TileBase tile = tileMap.GetTile(tileLocation);
                CollisionTable[y, x] = (tile == null ? false : true);
                tileLocation.x++;
            }
            tileLocation.y++;
        }
    }

    public bool CheckCollision(int _x, int _y)
    {

        if (CollisionTable == null)
        {
            return false;
        }

        int x = _x - tileMapLocation.x;
        int y = _y - tileMapLocation.y;

        if (y >= 0 && y < CollisionTable.GetLength(0))
        {
            if (x >= 0 && x < CollisionTable.GetLength(1))
            {
                return (CollisionTable[y, x] == true);
            }
        }
        return false;
    }
}

