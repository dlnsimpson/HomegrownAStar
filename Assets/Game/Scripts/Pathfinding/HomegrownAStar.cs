﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomegrownAStar : Singleton<HomegrownAStar>
{
    public CollisionMap CollisionMap;
    [Tooltip("Where is 0,0 on the map?")]
    public MapOriginEnum MapOrigin = MapOriginEnum.Center;
    [Tooltip("Method for calculating distance")]
    public DistanceCalcEnum DistanceCalc = DistanceCalcEnum.Euclidean;
    
    public enum DistanceCalcEnum
    {
        Euclidean,
        Manhattan
    };

    public enum MapOriginEnum
    {
        Center,
        //Future use
        //TopLeft,
        //BottomLeft,
        //TopRight,
        //BottomRight
    };

    private List<Node> openList = new List<Node>();
    private List<Vector3Int> moves = new List<Vector3Int>();
    private Dictionary<long, Node> closedList = new Dictionary<long, Node>();
    private Vector3Int goal;
    private int initialGCost = 0;

    private void Awake()
    {
        //Here we add all the directions we'd like to evaluate
        moves.Add(Vector3Int.up);
        moves.Add(Vector3Int.down);
        moves.Add(Vector3Int.left);
        moves.Add(Vector3Int.right);
    }

    public Vector3 generatePath(Vector3 _startPosition, Vector3 _targetPosition, List<Vector3> _path)
    {
        //Just return the target if the collision table is null
        if (CollisionMap == null || CollisionMap.CollisionTable == null)
        {
            return _targetPosition;
        }

        //We correct for the edges of the tilemap, if needed
        _targetPosition.x = _targetPosition.x < CollisionMap.tileMapCellBounds.xMin ? CollisionMap.tileMapCellBounds.xMin : _targetPosition.x;
        _targetPosition.x = _targetPosition.x >= CollisionMap.tileMapCellBounds.xMax ? CollisionMap.tileMapCellBounds.xMax : _targetPosition.x;
        _targetPosition.y = _targetPosition.y < CollisionMap.tileMapCellBounds.yMin ? CollisionMap.tileMapCellBounds.yMin : _targetPosition.y;
        _targetPosition.y = _targetPosition.y >= CollisionMap.tileMapCellBounds.yMax ? CollisionMap.tileMapCellBounds.yMax : _targetPosition.y;

        //Clear the path List in case there's anything hanging around
        _path.Clear();

        //Clear the open and closed lists, too
        clearLists();

        //Convert both start and finish to Vector3 int
        goal = new Vector3Int((int)_targetPosition.x, (int)_targetPosition.y, (int)_targetPosition.z);
        Vector3Int start = new Vector3Int((int)_startPosition.x, (int)_startPosition.y, (int)_startPosition.z);

        //We create a Node object from the data passed in. Parent is null because this is the first node.
        Node n = new Node(null, start, initialGCost, getDistance(start, goal));

        //Add our new node to the open list so we can evaluate it
        openList.Add(n);

        //Sort our list of nodes using the totalCost as the comparator
        openList.Sort((System.Comparison<Node>)((x, y) => x.totalCost().CompareTo(y.totalCost())));

        Node thisNode = null;

        //As long as there's stuff in the open list, we keep going. 
        while (openList.Count > 0)
        {
            Node nodeA = openList[0];
            thisNode = processNode(nodeA);
            if (thisNode != null)
            {
                //This means that processNode found the goal node, so we stop iterating
                break;
            }
        }

        //This will build out the _path List, with the very last node as element 0 and
        //working its way back through all the parents to the first node we created.
        //We'll know we hit the origin node when its parent is null.
        while (thisNode != null)
        {
            _path.Add(thisNode.location);
            thisNode = thisNode.parent;
        }

        //We have to reverse the _path List because we built it upside down
        _path.Reverse();
        clearLists();

        return _targetPosition;
    }

    private void clearLists()
    {
        openList.Clear();
        closedList.Clear();
    }

    private int getDistance(Vector3Int pointA, Vector3Int pointB)
    {
        //This uses the distance calc mechanism set in the inspector to determine Euclidean or Manhattan
        if (DistanceCalc == DistanceCalcEnum.Euclidean)
        {
            return getEuclideanDistance(pointA, pointB);
        }
        else
        {
            return getManhattanDistance(pointA, pointB);
        }
    }

    private int getEuclideanDistance(Vector3Int pointA, Vector3Int pointB)
    {
        //Plain old Unity Distance is calculating Euclidean
        return (int)Vector3Int.Distance(pointA, pointB);
    }

    private int getManhattanDistance(Vector3Int pointA, Vector3Int pointB)
    {
        //Simple formula for Manhattan
        return Mathf.Abs(pointA.x - pointB.x) + Mathf.Abs(pointA.y - pointB.y);
    }

    public long nodeKey(Vector3Int location)
    {
        return location.GetHashCode();
    }

    private Node findOpenNodeByPosition(Vector3 _thisPosition)
    {
        //We look at each node in the open list to see if any locations match the passed-in position
        foreach (Node n in openList)
        {
            if (n.location.x == _thisPosition.x && n.location.y == _thisPosition.y)
            {
                return n;
            }
        }
        //Nothing matched, so we return null
        return null;
    }

    private bool locationsEqual(Vector3Int pointA, Vector3Int pointB)
    {
        //We check to see if points A and B are functionally equivalent
        if ((pointA.x == pointB.x) && (pointA.y == pointB.y))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool boundsCheck(int _value, int totalDimension)
    {
        //Future use -- add ability to handle different origins
        //For now, we're just dealing with the center of the map at 0,0

        int min = 0;
        int max = 0;

        if (MapOrigin == MapOriginEnum.Center)
        {
            min = -(int)totalDimension / 2;
            max = (int)totalDimension / 2;
        }
        if (_value > min && _value <= max)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private Node processNode(Node _node)
    {
        //We're now visiting the node, so we remove it from the open list
        openList.Remove(_node);

        //First we check to see if the closed list already knows about this location

        if (!closedList.ContainsKey(nodeKey(_node.location)))
        {
            //The closed list didn't contain this node and location before, but now it WILL
            closedList[nodeKey(_node.location)] = _node;

            if (locationsEqual(_node.location, goal))
            {
                //Are we at the goal? Yes, return the passed-in node, which will stop the outer loop in the calling method
                return _node;
            }
        }
        //Here we're going to run through the possible moves from the current node
        foreach (Vector3Int move in moves)
        {
            //Move by one square and evaluate our options
            int newX = _node.location.x + move.x;
            int newY = _node.location.y + move.y;

            Vector3Int newLocation = new Vector3Int(newX, newY, 0);

            //Is this location outside the map?
            if (!boundsCheck(newLocation.x, CollisionMap.MapWidth) && boundsCheck(newLocation.y, CollisionMap.MapHeight))
            {
                //It's outside, so just skip this whole thing and go to the next move in the list
                continue;
            }
            //Now we check it on the collision map
            if (CollisionMap.CheckCollision(newLocation.x, newLocation.y))
            {
                //We found a collision, so we check to see if the new location is the same as the goal
                if (locationsEqual(newLocation, goal))
                {
                    //We found the goal, and returning a non-null node to the calling method will stop its loop
                    return _node;
                }
            }
            else
            {
                //Does the closed list know about this location?
                if (!closedList.ContainsKey(nodeKey(newLocation)))
                {
                    //No, it's not on the closed list, so we check to see if it's on the open list
                    Node newNode = findOpenNodeByPosition(newLocation);

                    if (newNode == null)
                    {
                        //The open list doesn't know about it, so we create a new node with the new location,
                        //gCost of the old node + distance between old & new, and distance from new to goal

                        Node nextNode = new Node(_node, newLocation, getDistance(_node.location, newLocation) + _node.gCost, getDistance(newLocation, goal));

                        //We add this new node to the open list
                        openList.Add(nextNode);

                        //And now we sort the open list using the totalCost as the comparator
                        openList.Sort((System.Comparison<Node>)((x, y) => x.totalCost().CompareTo(y.totalCost())));
                    }
                    else
                    {
                        //The open list DOES know about the new location, so now we have to figure out if it's worth dealing with
                        int cost = getDistance(_node.location, newLocation) + _node.gCost;

                        if (cost < newNode.gCost)
                        {
                            newNode.gCost = cost;
                            openList.Sort((System.Comparison<Node>)((x, y) => x.totalCost().CompareTo(y.totalCost())));
                        }
                    }
                }
            }
        }
        return null;
    }

    class Node
    {
        public Vector3Int location;
        public Node parent;
        public int gCost;
        public int hCost;

        public Node(Node _parent, Vector3Int _location, int _gCost, int _hCost)
        {
            this.location = _location;
            this.parent = _parent;
            this.gCost = _gCost;
            this.hCost = _hCost;
        }

        public int totalCost()
        {
            return this.gCost + this.hCost;
        }
    }
}


